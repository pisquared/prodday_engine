"""
Copied and modified from https://github.com/EpocDotFr/todotxtio/blob/master/todotxtio.py
"""
import re

todo_data_regex = re.compile('^(?:(x) )?(?:(\d{4}-\d{2}-\d{2}) )?(?:\(([A-Z])\) )?(?:(\d{4}-\d{2}-\d{2}) )?')
todo_project_regex = re.compile(' \+(\S*)')
todo_context_regex = re.compile(' @(\S*)')
todo_tag_regex = re.compile(' (\S*):(\S*)')


class Todo(object):
    """Represent one todo.
    :param str text: The text of the todo
    :param bool completed: Should this todo be marked as completed?
    :param str completion_date: A date of completion, in the ``YYYY-MM-DD`` format. Setting this property will automatically set the ``completed`` attribute to ``True``.
    :param str priority: The priority of the todo represented by a char between ``A-Z``
    :param str creation_date: A date of creation, in the ``YYYY-MM-DD`` format
    :param list projects: A list of projects without leading ``+``
    :param list contexts: A list of projects without leading ``@``
    :param dict tags: A dict of tags
    """
    text = None
    completed = False
    completion_date = None
    priority = None
    creation_date = None
    projects = []
    contexts = []
    tags = {}

    def __init__(self, text=None, completed=False, completion_date=None, priority=None, creation_date=None,
                 projects=None, contexts=None, tags=None):
        self.text = text
        self.completed = completed

        if completion_date and self.completed:
            self.completion_date = completion_date

        self.priority = priority
        self.creation_date = creation_date
        self.projects = projects
        self.contexts = contexts
        self.tags = tags


def from_string(string):
    """Load a todo list from a string.
    :param str string: The string to parse
    :rtype: list
    """
    todos = []

    for line in string.strip().splitlines():
        line = line.strip()

        todo_pre_data = todo_data_regex.match(line)

        todo = Todo()

        if todo_pre_data:
            todo.completed = todo_pre_data.group(1) == 'x'

            if todo.completed:
                todo.creation_date = todo_pre_data.group(4)

                if todo_pre_data.group(2):
                    todo.completion_date = todo_pre_data.group(2)
            else:
                todo.creation_date = todo_pre_data.group(2)

            todo.priority = todo_pre_data.group(3)

            text = todo_data_regex.sub('', line).strip()
        else:
            text = line

        todo_projects = todo_project_regex.findall(text)

        if len(todo_projects) > 0:
            todo.projects = todo_projects
            text = todo_project_regex.sub('', text).strip()

        todo_contexts = todo_context_regex.findall(text)

        if len(todo_contexts) > 0:
            todo.contexts = todo_contexts
            text = todo_context_regex.sub('', text).strip()

        todo_tags = todo_tag_regex.findall(text)

        if len(todo_tags) > 0:
            for todo_tag in todo_tags:
                todo.tags[todo_tag[0]] = todo_tag[1]

            text = todo_tag_regex.sub('', text).strip()

        todo.text = text

        todos.append(todo)

    return todos
