import json
import os

from engine import app
from engine.views import client_bp
from pprint import pprint
from engine.store import db
import operator
from engine.models_orm import Model, Field, FieldType, Filter
from engine.app_models import View, Component
from flask import request, jsonify


def singularize(word):
    # TODO
    return word[:-1]


def create_model(definitions):
    models_serialized = []
    for model_def in definitions:
        model_name = model_def.get("name")
        fields_def = model_def.get("fields", [])
        fields = []
        for field_def in fields_def:
            field_label = field_def.get("label")
            field_type = field_def.get("type")
            field_options = field_def.get("options", {})
            fields.append(
                Field(label=field_label, type=field_type, options=field_options),
            )
        model = Model.create_model(model_name=model_name, fields=fields)
        models_serialized.append(model)
    return models_serialized

def create_view(definitions):
    for view_def in definitions:
        view = View.create(
            name=view_def.get("name"),
            route=view_def.get("route"),
            layout=view_def.get("layout"),
        )
        for component_def in view_def.get("components"):
            component = Component.create(
                name=component_def.get("name"),
                template=component_def.get("template"),
                options=component_def.get("options"),
                filters=component_def.get("filters"),
                view=view,
            )



def create_model_instance(definitions):
    instances_serialized = []
    for instance_def in definitions:
        model_name = instance_def.get("model")
        fields_def = instance_def.get("fields", [])
        fields = []
        for field_def in fields_def:
            field_label = field_def.get("label")
            field_value = field_def.get("value")
            fields.append(
                Field(label=field_label, value=field_value),
            )
        instance = Model.create_instance(model_name=model_name, fields=fields)
        instances_serialized.append(instance)
    return instances_serialized


@app.route('/api/v1/models/create', methods=["POST"])
def models_create():
    definitions = request.json.get("models", [])
    models_serialized = create_model(definitions)
    return jsonify({
        "models": models_serialized
    })


@app.route('/api/v1/instances', methods=["GET", "POST"])
def model_instance_get_create():
    if request.method == "POST":
        definitions = request.json.get("instances", [])
        instances_serialized = create_model_instance(definitions)
        return jsonify({
            "instances": instances_serialized
        })


META_MODEL_METHODS = {
    "models": create_model,
    "instances": create_model_instance,
    "views": create_view,
}

basepath = os.path.dirname(os.path.realpath(__file__))
appspath = os.path.join(basepath, "apps")

dbfile = os.path.join(basepath, "db.sqlite")


def test():
    with app.app_context():
        if not os.path.isfile(dbfile):
            db.create_all()

            tasks_app = os.path.join(appspath, "sunme", "tasks")

            for meta_model in ["models", "instances", "views"]:
                with open(os.path.join(tasks_app, "{}.json".format(meta_model))) as f:
                    data = json.load(f)
                    method = META_MODEL_METHODS[meta_model]
                    method(data.get(meta_model, []))


            #######################

            pprint("===================================================")
            first_task_by_id = Model.query(model_name="Task", model_id=1)
            pprint("first_task_by_id")
            pprint(first_task_by_id)
            pprint("===================================================")

            first_task_by_filter = Model.query(model_name="Task", filters=[
                Filter("is_done", operator.eq, "False"),
            ])
            pprint("first_task_by_filter")
            pprint(first_task_by_filter)

            none_tasks_are_done = Model.query(model_name="Task", filters=[
                Filter("is_done", operator.eq, "True"),
            ])
            pprint("none_tasks_are_done")
            pprint(none_tasks_are_done)
            pprint("===================================================")

            Model.update(model_name="Task", filters=[
                Filter("is_done", operator.eq, "False")
            ], updates=[
                Field(label="is_done", value="True"),
            ])

            done_tasks = Model.query(model_name="Task", filters=[
                Filter("is_done", operator.eq, "True"),
            ])
            pprint("done_tasks")
            pprint(done_tasks)
            pprint("===================================================")

            none_tasks_are_not_done = Model.query(model_name="Task", filters=[
                Filter("is_done", operator.eq, "False"),
            ])
            pprint("none_tasks_are_not_done")
            pprint(none_tasks_are_not_done)
            pprint("===================================================")

def retrieve_view_func():
    return render_template(view.template)


if __name__ == "__main__":
    test()
    for view in View.get_all_by():
        client_bp.add_url_rule('/{route}'.format(route=view.route),
                               endpoint='{name}_retrieve_all'.format(name=view.route),
                               view_func=retrieve_view_func,
                               methods=['GET'])
    app.run(host="0.0.0.0", port=5001)
    
