import csv
import os

from flask import Flask, render_template

app = Flask(__name__)


def read_csv(csv_filename):
    with open(csv_filename) as csvfile:
        reader = csv.DictReader(csvfile)
        return [r for r in reader]


class Component(object):
    def __init__(self):
        self.ctx = {}

    def build_ctx(self):
        self.ctx = {}


class View(object):
    def __init__(self, template):
        self.template = template
        self.components = []

    def register_component(self, component):
        self.components.append(component)

    def build_ctx(self):
        for component in self.components:
            component.build_ctx()


#####################
# COMPONENTS
######################

class CategoriesComponent(Component):
    html_template = "components/categories_progress.html"

    def build_ctx(self):
        self.ctx = {
            "categories": read_csv(os.path.join('data', 'user_1', 'categories.csv'))
        }


#####################
# END COMPONENTS
######################


COMPONENTS = {
    "categories_progress": CategoriesComponent,
}


@app.route("/")
def home():
    home_view.build_ctx()
    return render_template(home_view.template, view=home_view)


if __name__ == "__main__":
    home_view = View("vertical_flow.html")
    home_view.register_component(CategoriesComponent())
    app.run(debug=True)
