#!/usr/bin/env bash
set -e

if [ -z "$1" ]; then
    echo "Please supply a migration message as first argument."
    exit 1;
fi
export FLASK_APP=webapp.py

if [ -z migrations/versions ]; then
  VER='001'
else
  LAST_VER=$(ls migrations/versions/*.py | cut -d'_' -f2 | sort -n | tail -1 | sed 's/^0*//')
  ((LAST_VER++))
  VER=$(printf "%03d" $LAST_VER)
fi

flask db migrate --message "$VER $1"
for i in migrations/versions/*.py; do
  sed -i "s/sqlalchemy_utils.types.choice.ChoiceType/sa.String/ " "$i";
done