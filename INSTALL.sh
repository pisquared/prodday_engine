#!/usr/bin/env bash

set -e
set +x

echo "Installing virtualenv"
echo "======================================="
sudo apt-get update
sudo apt-get install -y python-virtualenv

echo "Creating virtualenv"
echo "======================================="
virtualenv -p python3 venv
source venv/bin/activate

echo "Installing python packages"
echo "======================================="
pip install -r requirements.txt
pip install --ignore-installed setuptools==36.5.0  # fixes crash with babel

echo "Patching flask_sql_cache_core"
echo "======================================="
patch venv/lib/python3.5/site-packages/flask_sqlalchemy_cache/core.py provision/flask_sql_cache_core.patch

echo "Init db"
echo "======================================="
export FLASK_APP="webapp.py"
source venv/bin/activate && python -m flask db upgrade

echo "Populating db"
echo "======================================="
source venv/bin/activate && python populate.py
