"""003 trackers

Revision ID: 8bc56c131956
Revises: 4c82c7588199
Create Date: 2018-07-01 11:41:13.649286

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8bc56c131956'
down_revision = '4c82c7588199'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('metric',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('filter_by', sa.Unicode(), nullable=True),
    sa.Column('aggregate_func', sa.String(length=255), nullable=True),
    sa.Column('aggregate_by', sa.String(length=255), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tracker',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('api_key', sa.Unicode(), nullable=True),
    sa.Column('thing_k', sa.Unicode(), nullable=True),
    sa.Column('metric_1_k', sa.Unicode(), nullable=True),
    sa.Column('metric_2_k', sa.Unicode(), nullable=True),
    sa.Column('metric_3_k', sa.Unicode(), nullable=True),
    sa.Column('metric_1_t', sa.String(length=255), nullable=True),
    sa.Column('metric_2_t', sa.String(length=255), nullable=True),
    sa.Column('metric_3_t', sa.String(length=255), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tracker_session',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('started_ts', sa.DateTime(), nullable=True),
    sa.Column('finished_ts', sa.DateTime(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('metric_1_v', sa.Integer(), nullable=True),
    sa.Column('metric_2_v', sa.Integer(), nullable=True),
    sa.Column('metric_3_v', sa.Integer(), nullable=True),
    sa.Column('meta', sa.Unicode(), nullable=True),
    sa.Column('tracker_id', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['tracker_id'], ['tracker.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('tracker_session')
    op.drop_table('tracker')
    op.drop_table('metric')
    # ### end Alembic commands ###
