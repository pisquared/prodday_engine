from flask_babel import gettext

HELP_MESSAGE_INBOX_ENTRY = gettext("""
<p>This is your inbox. Put anything useless here. Free your goddamn mind.</p>
<p>If you want to be smart-ass about it and create a todo, prefix the thing with "todo:".</p>
<p>Example:</p>
<blockquote>
    todo: kill Bill
</blockquote>
""")

HELP_MESSAGE_TASK = gettext("""
<p>Tasks are things that you want to do.</p>
<blockquote>
    Do or do not. There is no try
    <footer>
        <cite>That small green motherfucker from that space movie</cite>
    </footer>
</blockquote>
""")