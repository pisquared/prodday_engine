import importlib
from flask import render_template
from engine.views import client_bp
from engine.app_models import Settings, View


def get_layout_ctx():
  settings = Settings.get_owned()[0],
  current_context = settings.last_context,
  views = current_context.views,
  return dict(
      settings=Settings.get_owned()[0],
      current_context=current_context,
      views=views,
  )


@client_bp.route('/')
def home():
  layout_ctx = get_layout_ctx()
  return render_template("layout.html",
                         layout_ctx=layout_ctx,
                         )


@client_bp.route('/views/<int:view_id>')
def get_view(view_id):
  layout_ctx = get_layout_ctx()
  view = View.get_by(id=view_id)
  components = view.components
  for component in components:
    pass
    #my_module = importlib.import_module('apps.{}.{}'.format(component.name))
    #component.ctx = (component.name, build_component_context)()

  return render_template("view.html",
                         layout_ctx=layout_ctx,
                         view=view,
                         components=components)
