from engine.model_controller import ModelController, Ownable, Datable
from engine.store import db


class App(db.Model, ModelController, Datable):
  developer = db.Column(db.Unicode)
  name = db.Column(db.Unicode)


class AppView(db.Model, ModelController, Datable):
  name = db.Column(db.Unicode)

  layout = db.Column(db.Unicode)

  options = db.Column(db.Unicode)

  filters = db.Column(db.Unicode)

  app_id = db.Column(db.Integer, db.ForeignKey('app.id'))
  app = db.relationship("App", backref="views")


class AppComponent(db.Model, ModelController, Datable):
  name = db.Column(db.Unicode)
  template = db.Column(db.Unicode)

  layout = db.Column(db.Unicode)

  options = db.Column(db.Unicode)

  filters = db.Column(db.Unicode)

  app_id = db.Column(db.Integer, db.ForeignKey('app.id'))
  app = db.relationship("App", backref="components")


############ PER USER

class InstalledApp(db.Model, ModelController, Ownable, Datable):
  name = db.Column(db.Unicode)


class Context(db.Model, ModelController, Ownable, Datable):
  name = db.Column(db.Unicode)

  filters = db.Column(db.Unicode)


class View(db.Model, ModelController, Ownable, Datable):
  name = db.Column(db.Unicode)
  route = db.Column(db.Unicode)
  layout = db.Column(db.Unicode)

  options = db.Column(db.Unicode)

  filters = db.Column(db.Unicode)

  context_id = db.Column(db.Integer, db.ForeignKey('context.id'))
  context = db.relationship("Context", backref="views")


class Component(db.Model, ModelController, Ownable, Datable):
  name = db.Column(db.Unicode)
  template = db.Column(db.Unicode)

  options = db.Column(db.Unicode)

  filters = db.Column(db.Unicode)

  view_id = db.Column(db.Integer, db.ForeignKey('view.id'))
  view = db.relationship("View", backref="components")


class Settings(db.Model, ModelController, Ownable, Datable):
  last_context_id = db.Column(db.Integer, db.ForeignKey('context.id'))
  last_context = db.relationship("Context")
