import operator
from engine.models_orm import Model, Filter


def build_component_context(filters=None, *args, **kwargs):
  undone_tasks = Model.query(model_name="Task", filters=[
      Filter("is_done", operator.eq, "False"),
  ])
  return {
      "undone_tasks": undone_tasks,
  }
